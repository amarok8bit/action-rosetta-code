1. 100 doors: [task](http://www.rosettacode.org/wiki/100_doors) and [solution](source/100_doors.act)\
![](images/100_doors.png)

1. 15 puzzle game: [task](http://www.rosettacode.org/wiki/15_puzzle_game) and [solution](source/15_puzzle_game.act)\
![](images/15_puzzle_game.png)

1. 99 bottles of beer: [task](http://www.rosettacode.org/wiki/99_bottles_of_beer) and [solution](source/99_bottles_of_beer.act)\
![](images/99_bottles_of_beer.png)

1. A+B: [task](http://www.rosettacode.org/wiki/A%2BB) and [solution](source/A+B.act)\
![](images/A+B.png)

1. ABC problem: [task](http://www.rosettacode.org/wiki/ABC_problem) and [solution](source/ABC_problem.act)\
![](images/ABC_problem.png)

1. ABC words: [task](http://www.rosettacode.org/wiki/ABC_words) and [solution](source/ABC_words.act)\
![](images/ABC_words.png)

1. Abundant, deficient and perfect number classifications: [task](http://www.rosettacode.org/wiki/Abundant,_deficient_and_perfect_number_classifications) and [solution1](source/Abundant,_deficient_and_perfect_number_classifications.act) [solution2](source/Abundant,_deficient_and_perfect_number_classifications_v2.act)\
![](images/Abundant,_deficient_and_perfect_number_classifications_v2.png)

1. Ackermann function: [task](http://www.rosettacode.org/wiki/Ackermann_function) and [solution](source/Ackermann_function.act)\
![](images/Ackermann_function.png)

1. Address of a variable: [task](http://www.rosettacode.org/wiki/Address_of_a_variable) and [solution](source/Address_of_a_variable.act)\
![](images/Address_of_a_variable.png)

1. Align columns: [task](http://www.rosettacode.org/wiki/Align_columns) and [solution](source/Align_columns.act)\
![](images/Align_columns.png)

1. Almost prime: [task](http://www.rosettacode.org/wiki/Almost_prime) and [solution](source/Almost_prime.act)\
![](images/Almost_prime.png)

1. Amicable pairs: [task](http://www.rosettacode.org/wiki/Amicable_pairs) and [solution](source/Amicable_pairs.act)\
![](images/Amicable_pairs.png)

1. Angle difference between two bearings: [task](http://www.rosettacode.org/wiki/Angle_difference_between_two_bearings) and [solution](source/Angle_difference_between_two_bearings.act)\
![](images/Angle_difference_between_two_bearings.png)

1. Animation: [task](http://www.rosettacode.org/wiki/Animation) and [solution](source/Animation.act)\
![](images/Animation.png)

1. Anti-primes: [task](http://www.rosettacode.org/wiki/Anti-primes) and [solution](source/Anti-primes.act)\
![](images/Anti-primes.png)

1. Append a record to the end of a text file: [task](http://www.rosettacode.org/wiki/Append_a_record_to_the_end_of_a_text_file) and [solution](source/Append_a_record_to_the_end_of_a_text_file.act)\
![](images/Append_a_record_to_the_end_of_a_text_file.png)

1. Archimedean spiral: [task](http://www.rosettacode.org/wiki/Archimedean_spiral) and [solution](source/Archimedean_spiral.act)\
![](images/Archimedean_spiral.png)

1. Arithmetic-geometric mean: [task](http://www.rosettacode.org/wiki/Arithmetic-geometric_mean) and [solution](source/Arithmetic-geometric_mean.act)\
![](images/Arithmetic-geometric_mean.png)

1. Arithmetic/Complex: [task](http://www.rosettacode.org/wiki/Arithmetic/Complex) and [solution](source/Complex.act)\
![](images/Complex.png)

1. Arithmetic/Integer: [task](http://www.rosettacode.org/wiki/Arithmetic/Integer) and [solution](source/Arithmetic_Integer.act)\
![](images/Arithmetic_Integer.png)

1. Arithmetic/Rational: [task](http://www.rosettacode.org/wiki/Arithmetic/Rational) and [solution](source/Rational.act)\
![](images/Rational.png)

1. Array concatenation: [task](http://www.rosettacode.org/wiki/Array_concatenation) and [solution](source/Array_concatenation.act)\
![](images/Array_concatenation.png)

1. Arrays: [task](http://www.rosettacode.org/wiki/Arrays) and [solution](source/Arrays.act)\
![](images/Arrays.png)

1. Attractive numbers: [task](http://www.rosettacode.org/wiki/Attractive_numbers) and [solution](source/Attractive_numbers.act)\
![](images/Attractive_numbers.png)

1. Averages/Arithmetic mean: [task](http://www.rosettacode.org/wiki/Averages/Arithmetic_mean) and [solution](source/Arithmetic_mean.act)\
![](images/Arithmetic_mean.png)

1. Averages/Mean time of day: [task](http://www.rosettacode.org/wiki/Averages/Mean_time_of_day) and [solution](source/Mean_time_of_day.act)\
![](images/Mean_time_of_day.png)

1. Averages/Median: [task](http://www.rosettacode.org/wiki/Averages/Median) and [solution](source/Averages_Median.act)\
![](images/Averages_Median.png)

1. Averages/Mode: [task](http://www.rosettacode.org/wiki/Averages/Mode) and [solution](source/Mode.act)\
![](images/Mode.png)

1. Averages/Pythagorean means: [task](http://www.rosettacode.org/wiki/Averages/Pythagorean_means) and [solution](source/Pythagorean_means.act)\
![](images/Pythagorean_means.png)

1. Averages/Root mean square: [task](http://www.rosettacode.org/wiki/Averages/Root_mean_square) and [solution](source/Root_mean_square.act)\
![](images/Root_mean_square.png)

1. Balanced brackets: [task](http://www.rosettacode.org/wiki/Balanced_brackets) and [solution](source/Balanced_brackets.act)\
![](images/Balanced_brackets.png)

1. Barnsley fern: [task](http://www.rosettacode.org/wiki/Barnsley_fern) and [solution](source/Barnsley_fern.act)\
![](images/Barnsley_fern.png)

1. Base 16 numbers needing a to f: [task](http://www.rosettacode.org/wiki/Base_16_numbers_needing_a_to_f) and [solution](source/Base_16_numbers_needing_a_to_f.act)\
![](images/Base_16_numbers_needing_a_to_f.png)

1. Base64 decode data: [task](http://www.rosettacode.org/wiki/Base64_decode_data) and [solution](source/Base64_decode_data.act)\
![](images/Base64_decode_data.png)

1. Base64 encode data: [task](http://www.rosettacode.org/wiki/Base64_encode_data) and [solution](source/Base64_encode_data.act)\
![](images/Base64_encode_data.png)

1. Best shuffle: [task](http://www.rosettacode.org/wiki/Best_shuffle) and [solution](source/Best_shuffle.act)\
![](images/Best_shuffle.png)

1. Bilinear interpolation: [task](http://www.rosettacode.org/wiki/Bilinear_interpolation) and [solution1](source/Bilinear_interpolation.act) [solution2](source/LoadPPM5.act)\
![](images/Bilinear_interpolation.png)

1. Bin given limits: [task](http://www.rosettacode.org/wiki/Bin_given_limits) and [solution](source/Bin_given_limits.act)\
![](images/Bin_given_limits.png)

1. Binary digits: [task](http://www.rosettacode.org/wiki/Binary_digits) and [solution](source/Binary_digits.act)\
![](images/Binary_digits.png)

1. Binary search: [task](http://www.rosettacode.org/wiki/Binary_search) and [solution](source/Binary_search.act)\
![](images/Binary_search.png)

1. Bioinformatics/base count: [task](http://www.rosettacode.org/wiki/Bioinformatics/base_count) and [solution](source/Bioinformatics_base_count.act)\
![](images/Bioinformatics_base_count.png)

1. Bioinformatics/Subsequence: [task](http://www.rosettacode.org/wiki/Bioinformatics/Subsequence) and [solution](source/Bioinformatics_subsequence.act)\
![](images/Bioinformatics_subsequence.png)

1. Bitmap: [task](http://www.rosettacode.org/wiki/Bitmap) and [solution1](source/Bitmap.act) [solution2](source/RgbImage.act)\
![](images/Bitmap.png)

1. Bitmap/Bézier curves/Cubic: [task](http://www.rosettacode.org/wiki/Bitmap/B%C3%A9zier_curves/Cubic) and [solution](source/Bézier_curves_cubic.act)\
![](images/Bézier_curves_cubic.png)

1. Bitmap/Bézier curves/Quadratic: [task](http://www.rosettacode.org/wiki/Bitmap/B%C3%A9zier_curves/Quadratic) and [solution](source/Bézier_curves_quadratic.act)\
![](images/Bézier_curves_quadratic.png)

1. Bitmap/Bresenham's line algorithm: [task](http://www.rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm) and [solution1](source/Bresenham's_line_algorithm.act) [solution2](source/RgbLine.act)\
![](images/Bresenham's_line_algorithm.png)

1. Bitmap/Flood fill: [task](http://www.rosettacode.org/wiki/Bitmap/Flood_fill) and [solution](source/Flood_fill.act)\
![](images/Flood_fill.png)

1. Bitmap/Histogram: [task](http://www.rosettacode.org/wiki/Bitmap/Histogram) and [solution](source/Bitmap_Histogram.act)\
![](images/Bitmap_Histogram.png)

1. Bitmap/Midpoint circle algorithm: [task](http://www.rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm) and [solution1](source/Midpoint_circle_algorithm.act) and [solution2](source/RgbCircl.act)\
![](images/Midpoint_circle_algorithm.png)

1. Bitmap/Read a PPM file: [task](http://www.rosettacode.org/wiki/Bitmap/Read_a_PPM_file) and [solution](source/Read_a_PPM_file.act)\
![](images/Read_a_PPM_file.png)

1. Bitmap/Write a PPM file: [task](http://www.rosettacode.org/wiki/Bitmap/Write_a_PPM_file) and [solution](source/Write_a_PPM_file.act)\
![](images/Write_a_PPM_file.png)

1. Bitwise operations: [task](http://www.rosettacode.org/wiki/Bitwise_operations) and [solution](source/Bitwise_operations.act)\
![](images/Bitwise_operations.png)

1. Boolean values: [task](http://www.rosettacode.org/wiki/Boolean_values) and [solution](source/Boolean_values.act)\
![](images/Boolean_values.png)

1. Box the compass: [task](http://www.rosettacode.org/wiki/Box_the_compass) and [solution](source/Box_the_compass.act)\
![](images/Box_the_compass.png)

1. Brazilian numbers: [task](http://www.rosettacode.org/wiki/Brazilian_numbers) and [solution](source/Brazilian_numbers.act)\
![](images/Brazilian_numbers.png)

1. Brownian tree: [task](http://www.rosettacode.org/wiki/Brownian_tree) and [solution](source/Brownian_tree.act)\
![](images/Brownian_tree.png)

1. Bulls and cows: [task](http://www.rosettacode.org/wiki/Bulls_and_cows) and [solution](source/Bulls_and_cows.act)\
![](images/Bulls_and_cows.png)

1. Caesar cipher: [task](http://www.rosettacode.org/wiki/Caesar_cipher) and [solution](source/Caesar_cipher.act)\
![](images/Caesar_cipher.png)

1. Calculating the value of e: [task](http://www.rosettacode.org/wiki/Calculating_the_value_of_e) and [solution](source/Calculating_the_value_of_e.act)\
![](images/Calculating_the_value_of_e.png)

1. Cantor set: [task](http://www.rosettacode.org/wiki/Cantor_set) and [solution](source/Cantor_set.act)\
![](images/Cantor_set.png)

1. Cartesian product of two or more lists: [task](http://www.rosettacode.org/wiki/Cartesian_product_of_two_or_more_lists) and [solution](source/Cartesian_product_of_two_or_more_lists.act)\
![](images/Cartesian_product_of_two_or_more_lists.png)

1. Case-sensitivity of identifiers: [task](http://www.rosettacode.org/wiki/Case-sensitivity_of_identifiers) and [solution](source/Case-sensitivity_of_identifiers.act)\
![](images/Case-sensitivity_of_identifiers.png)

1. Casting out nines: [task](http://www.rosettacode.org/wiki/Casting_out_nines) and [solution](source/Casting_out_nines.act)\
![](images/Casting_out_nines.png)

1. Catalan numbers: [task](http://www.rosettacode.org/wiki/Catalan_numbers) and [solution](source/Catalan_numbers.act)\
![](images/Catalan_numbers.png)

1. Catalan numbers/Pascal's triangle: [task](http://www.rosettacode.org/wiki/Catalan_numbers/Pascal%27s_triangle) and [solution](source/Catalan_numbers_Pascal's_triangle.act)\
![](images/Catalan_numbers_Pascal's_triangle.png)

1. Chaos game: [task](http://www.rosettacode.org/wiki/Chaos_game) and [solution](source/Chaos_game.act)\
![](images/Chaos_game.png)

1. Character codes: [task](http://www.rosettacode.org/wiki/Character_codes) and [solution](source/Character_codes.act)\
![](images/Character_codes.png)

1. Check that file exists: [task](http://www.rosettacode.org/wiki/Check_that_file_exists) and [solution](source/Check_that_file_exists.act)\
![](images/Check_that_file_exists.png)

1. Chinese remainder theorem: [task](http://www.rosettacode.org/wiki/Chinese_remainder_theorem) and [solution](source/Chinese_remainder_theorem.act)\
![](images/Chinese_remainder_theorem.png)

1. Chinese zodiac: [task](http://www.rosettacode.org/wiki/Chinese_zodiac) and [solution](source/Chinese_zodiac.act)\
![](images/Chinese_zodiac.png)

1. Circles of given radius through two points: [task](http://www.rosettacode.org/wiki/Circles_of_given_radius_through_two_points) and [solution](source/Circles_of_given_radius_through_two_points.act)\
![](images/Circles_of_given_radius_through_two_points.png)

1. Cistercian numerals: [task](http://www.rosettacode.org/wiki/Cistercian_numerals) and [solution](source/Cistercian_numerals.act)\
![](images/Cistercian_numerals.png)

1. Color of a screen pixel: [task](http://www.rosettacode.org/wiki/Color_of_a_screen_pixel) and [solution](source/Color_of_a_screen_pixel.act)\
![](images/Color_of_a_screen_pixel.png)

1. Colour bars/Display: [task](http://www.rosettacode.org/wiki/Colour_bars/Display) and [solution](source/Colour_bars_display.act)\
![](images/Colour_bars_display.png)

1. Colour pinstripe/Display: [task](http://www.rosettacode.org/wiki/Colour_pinstripe/Display) and [solution](source/Colour_pinstripe_display.act)\
![](images/Colour_pinstripe_display.png)

1. Combinations: [task](http://www.rosettacode.org/wiki/Combinations) and [solution](source/Combinations.act)\
![](images/Combinations.png)

1. Combinations with repetitions: [task](http://www.rosettacode.org/wiki/Combinations_with_repetitions) and [solution](source/Combinations_with_repetitions.act)\
![](images/Combinations_with_repetitions.png)

1. Comma quibbling: [task](http://www.rosettacode.org/wiki/Comma_quibbling) and [solution](source/Comma_quibbling.act)\
![](images/Comma_quibbling.png)

1. Comments: [task](www.rosettacode.org/wiki/Comments) and [solution](source/Comments.act)\
![](images/Comments.png)

1. Common list elements: [task](http://www.rosettacode.org/wiki/Common_list_elements) and [solution](source/Common_list_elements.act)\
![](images/Common_list_elements.png)

1. Common sorted list: [task](http://www.rosettacode.org/wiki/Common_sorted_list) and [solution](source/Common_sorted_list.act)\
![](images/Common_sorted_list.png)

1. Compare a list of strings: [task](http://www.rosettacode.org/wiki/Compare_a_list_of_strings) and [solution](source/Compare_a_list_of_strings.act)\
![](images/Compare_a_list_of_strings.png)

1. Compound data type: [task](http://www.rosettacode.org/wiki/Compound_data_type) and [solution](source/Compound_data_type.act)\
![](images/Compound_data_type.png)

1. Concatenate two primes is also prime: [task](http://www.rosettacode.org/wiki/Concatenate_two_primes_is_also_prime) and [solution](source/Concatenate_two_primes_is_also_prime.act)\
![](images/Concatenate_two_primes_is_also_prime.png)

1. Conditional structures: [task](http://www.rosettacode.org/wiki/Conditional_structures) and [solution](source/Conditional_structures.act)\
![](images/Conditional_structures.png)

1. Constrained random points on a circle: [task](http://www.rosettacode.org/wiki/Constrained_random_points_on_a_circle) and [solution](source/Constrained_random_points_on_a_circle.act)\
![](images/Constrained_random_points_on_a_circle.png)

1. Continued fraction: [task](http://www.rosettacode.org/wiki/Continued_fraction) and [solution](source/Continued_fraction.act)\
![](images/Continued_fraction.png)

1. Convert seconds to compound duration: [task](http://www.rosettacode.org/wiki/Convert_seconds_to_compound_duration) and [solution](source/Convert_seconds_to_compound_duration.act)\
![](images/Convert_seconds_to_compound_duration.png)

1. Convex hull: [task](http://www.rosettacode.org/wiki/Convex_hull) and [solution](source/Convex_hull.act)\
![](images/Convex_hull.png)

1. Coprime triplets: [task](http://www.rosettacode.org/wiki/Coprime_triplets) and [solution](source/Coprime_triplets.act)\
![](images/Coprime_triplets.png)

1. Coprimes: [task](http://www.rosettacode.org/wiki/Coprimes) and [solution](source/Coprimes.act)\
![](images/Coprimes.png)

1. Copy a string: [task](http://www.rosettacode.org/wiki/Copy_a_string) and [solution](source/Copy_a_string.act)\
![](images/Copy_a_string.png)

1. Copy stdin to stdout: [task](http://www.rosettacode.org/wiki/Copy_stdin_to_stdout) and [solution](source/Copy_stdin_to_stdout.act)\
![](images/Copy_stdin_to_stdout.png)

1. Count how many vowels and consonants occur in a string: [task](http://www.rosettacode.org/wiki/Count_how_many_vowels_and_consonants_occur_in_a_string) and [solution](source/Count_how_many_vowels_and_consonants_occur_in_a_string.act)\
![](images/Count_how_many_vowels_and_consonants_occur_in_a_string.png)

1. Count in factors: [task](http://www.rosettacode.org/wiki/Count_in_factors) and [solution](source/Count_in_factors.act)\
![](images/Count_in_factors.png)

1. Count in octal: [task](http://www.rosettacode.org/wiki/Count_in_octal) and [solution](source/Count_in_octal.act)\
![](images/Count_in_octal.png)

1. Count occurrences of a substring: [task](http://www.rosettacode.org/wiki/Count_occurrences_of_a_substring) and [solution](source/Count_occurrences_of_a_substring.act)\
![](images/Count_occurrences_of_a_substring.png)

1. Cousin primes: [task](http://www.rosettacode.org/wiki/Cousin_primes) and [solution](source/Cousin_primes.act)\
![](images/Cousin_primes.png)

1. Create a file: [task](http://www.rosettacode.org/wiki/Create_a_file) and [solution](source/Create_a_file.act)\
![](images/Create_a_file.png)

1. Create a file on magnetic tape: [task](http://www.rosettacode.org/wiki/Create_a_file_on_magnetic_tape) and [solution](source/Create_a_file_on_magnetic_tape.act)\
![](images/Create_a_file_on_magnetic_tape.png)

1. Create a two-dimensional array at runtime: [task](http://www.rosettacode.org/wiki/Create_a_two-dimensional_array_at_runtime) and [solution](source/Create_a_two-dimensional_array_at_runtime.act)\
![](images/Create_a_two-dimensional_array_at_runtime.png)

1. Create an HTML table: [task](http://www.rosettacode.org/wiki/Create_an_HTML_table) and [solution](source/Create_an_HTML_table.act)\
![](images/Create_an_HTML_table.png)

1. Create an object at a given address: [task](http://www.rosettacode.org/wiki/Create_an_object_at_a_given_address) and [solution](source/Create_an_object_at_a_given_address.act)\
![](images/Create_an_object_at_a_given_address.png)

1. Cubic Special Primes: [task](http://www.rosettacode.org/wiki/Cubic_Special_Primes) and [solution](source/Cubic_Special_Primes.act)\
![](images/Cubic_Special_Primes.png)

1. Cumulative standard deviation: [task](http://www.rosettacode.org/wiki/Cumulative_standard_deviation) and [solution](source/Cumulative_standard_deviation.act)\
![](images/Cumulative_standard_deviation.png)

1. Curve that touches three points: [task](http://www.rosettacode.org/wiki/Curve_that_touches_three_points) and [solution](source/Curve_that_touches_three_points.act)\
![](images/Curve_that_touches_three_points.png)

1. CUSIP: [task](http://www.rosettacode.org/wiki/CUSIP) and [solution](source/CUSIP.act)\
![](images/CUSIP.png)

1. Damm algorithm: [task](http://www.rosettacode.org/wiki/Damm_algorithm) and [solution](source/Damm_algorithm.act)\
![](images/Damm_algorithm.png)

1. Date format: [task](http://www.rosettacode.org/wiki/Date_format) and [solution](source/Date_format.act)\
![](images/Date_format.png)

1. Days between dates: [task](http://www.rosettacode.org/wiki/Days_between_dates) and [solution](source/Days_between_dates.act)\
![](images/Days_between_dates.png)

1. Delete a file: [task](http://www.rosettacode.org/wiki/Delete_a_file) and [solution](source/Delete_a_file.act)\
![](images/Delete_a_file.png)

1. Department numbers: [task](www.rosettacode.org/wiki/Department_numbers) and [solution](source/Department_numbers.act)\
![](images/Department_numbers.png)

1. Determine if a string has all the same characters: [task](http://www.rosettacode.org/wiki/Determine_if_a_string_has_all_the_same_characters) and [solution](source/Determine_if_a_string_has_all_the_same_characters.act)\
![](images/Determine_if_a_string_has_all_the_same_characters.png)

1. Determine if a string has all unique characters: [task](http://www.rosettacode.org/wiki/Determine_if_a_string_has_all_unique_characters) and [solution](source/Determine_if_a_string_has_all_unique_characters.act)\
![](images/Determine_if_a_string_has_all_unique_characters.png)

1. Determine if a string is collapsible: [task](http://www.rosettacode.org/wiki/Determine_if_a_string_is_collapsible) and [solution](source/Determine_if_a_string_is_collapsible.act)\
![](images/Determine_if_a_string_is_collapsible.png)

1. Determine if a string is numeric: [task](http://www.rosettacode.org/wiki/Determine_if_a_string_is_numeric) and [solution1](source/Determine_if_a_string_is_numeric.act) [solution2](source/Determine_if_a_string_is_numeric_state_machine.act)\
![](images/Determine_if_a_string_is_numeric_state_machine.png)

1. Determine if a string is squeezable: [task](http://www.rosettacode.org/wiki/Determine_if_a_string_is_squeezable) and [solution](source/Determine_if_a_string_is_squeezable.act)\
![](images/Determine_if_a_string_is_squeezable.png)

1. Dice game probabilities: [task](http://www.rosettacode.org/wiki/Dice_game_probabilities) and [solution](source/Dice_game_probabilities.act)\
![](images/Dice_game_probabilities.png)

1. Distinct power numbers: [task](http://www.rosettacode.org/wiki/Distinct_power_numbers) and [solution](source/Distinct_power_numbers.act)\
![](images/Distinct_power_numbers.png)

1. Dot product: [task](http://www.rosettacode.org/wiki/Dot_product) and [solution](source/Dot_product.act)\
![](images/Dot_product.png)

1. Doubly-linked list/Definition: [task](http://www.rosettacode.org/wiki/Doubly-linked_list/Definition) and [solution](source/Doubly-linked_list_definition.act)\
![](images/Doubly-linked_list_definition.png)

1. Doubly-linked list/Element definition: [task](http://www.rosettacode.org/wiki/Doubly-linked_list/Element_definition) and [solution](source/Doubly-linked_list_element_definition.act)\
![](images/Doubly-linked_list_element_definition.png)

1. Doubly-linked list/Element insertion: [task](http://www.rosettacode.org/wiki/Doubly-linked_list/Element_insertion) and [solution](source/Doubly-linked_list_element_insertion.act)\
![](images/Doubly-linked_list_element_insertion.png)

1. Doubly-linked list/Element removal: [task](http://www.rosettacode.org/wiki/Doubly-linked_list/Element_removal) and [solution](source/Doubly-linked_list_element_removal.act)\
![](images/Doubly-linked_list_element_removal.png)

1. Doubly-linked list/Traversal: [task](http://www.rosettacode.org/wiki/Doubly-linked_list/Traversal) and [solution](source/Doubly-linked_list_traversal.act)\
![](images/Doubly-linked_list_traversal.png)

1. Dragon curve: [task](http://www.rosettacode.org/wiki/Dragon_curve) and [solution](source/Dragon_curve.act)\
![](images/Dragon_curve.png)

1. Draw a cuboid: [task](http://www.rosettacode.org/wiki/Draw_a_cuboid) and [solution](source/Draw_a_cuboid.act)\
![](images/Draw_a_cuboid.png)

1. Draw a pixel: [task](http://www.rosettacode.org/wiki/Draw_a_pixel) and [solution](source/Draw_a_pixel.act)\
![](images/Draw_a_pixel.png)

1. Draw a sphere: [task](http://www.rosettacode.org/wiki/Draw_a_sphere) and [solution](source/Draw_a_sphere.act)\
![](images/Draw_a_sphere.png)

1. Dutch national flag problem: [task](http://www.rosettacode.org/wiki/Dutch_national_flag_problem) and [solution](source/Dutch_national_flag_problem.act)\
![](images/Dutch_national_flag_problem.png)

1. Egyptian division: [task](http://www.rosettacode.org/wiki/Egyptian_division) and [solution](source/Egyptian_division.act)\
![](images/Egyptian_division.png)

1. EKG sequence convergence: [task](http://www.rosettacode.org/wiki/EKG_sequence_convergence) and [solution](source/EKG_sequence_convergence.act)\
![](images/EKG_sequence_convergence.png)

1. Elementary cellular automaton: [task](http://www.rosettacode.org/wiki/Elementary_cellular_automaton) and [solution](source/Elementary_cellular_automaton.act)\
![](images/Elementary_cellular_automaton.png)

1. Empty program: [task](http://www.rosettacode.org/wiki/Empty_program) and [solution](source/Empty_program.act)\
![](images/Empty_program.png)

1. Empty string: [task](http://www.rosettacode.org/wiki/Empty_string) and [solution](source/Empty_string.act)\
![](images/Empty_string.png)

1. Equilibrium index: [task](http://www.rosettacode.org/wiki/Equilibrium_index) and [solution](source/Equilibrium_index.act)\
![](images/Equilibrium_index.png)

1. Erdős-primes: [task](http://www.rosettacode.org/wiki/Erd%C5%91s-primes) and [solution](source/Erdős-primes.act)\
![](images/Erdős-primes.png)

1. Ethiopian multiplication: [task](http://www.rosettacode.org/wiki/Ethiopian_multiplication) and [solution](source/Ethiopian_multiplication.act)\
![](images/Ethiopian_multiplication.png)

1. Even or odd: [task](http://www.rosettacode.org/wiki/Even_or_odd) and [solution](source/Even_or_odd.act)\
![](images/Even_or_odd.png)

1. Execute HQ9+: [task](http://www.rosettacode.org/wiki/Execute_HQ9%2B) and [solution](source/Execute_HQ9+.act)\
![](images/Execute_HQ9+.png)

1. Exponentiation operator: [task](http://www.rosettacode.org/wiki/Exponentiation_operator) and [solution](source/Exponentiation_operator.act)\
![](images/Exponentiation_operator.png)

1. Exponentiation order: [task](http://www.rosettacode.org/wiki/Exponentiation_order) and [solution](source/Exponentiation_order.act)\
![](images/Exponentiation_order.png)

1. Extra primes: [task](http://www.rosettacode.org/wiki/Extra_primes) and [solution](source/Extra_primes.act)\
![](images/Extra_primes.png)

1. Extract file extension: [task](http://www.rosettacode.org/wiki/Extract_file_extension) and [solution](source/Extract_file_extension.act)\
![](images/Extract_file_extension.png)

1. Factorial: [task](http://www.rosettacode.org/wiki/Factorial) and [solution](source/Factorial.act)\
![](images/Factorial.png)

1. Factors of an integer: [task](http://www.rosettacode.org/wiki/Factors_of_an_integer) and [solution](source/Factors_of_an_integer.act)\
![](images/Factors_of_an_integer.png)

1. FASTA format: [task](http://www.rosettacode.org/wiki/FASTA_format) and [solution](source/FASTA_format.act)\
![](images/FASTA_format.png)

1. Fibonacci n-step number sequences: [task](http://www.rosettacode.org/wiki/Fibonacci_n-step_number_sequences) and [solution](source/Fibonacci_n-step_number_sequences.act)\
![](images/Fibonacci_n-step_number_sequences.png)

1. Fibonacci sequence: [task](http://www.rosettacode.org/wiki/Fibonacci_sequence) and [solution](source/Fibonacci_sequence.act)\
![](images/Fibonacci_sequence.png)

1. File extension is in extensions list: [task](http://www.rosettacode.org/wiki/File_extension_is_in_extensions_list) and [solution](source/File_extension_is_in_extensions_list.act)\
![](images/File_extension_is_in_extensions_list.png)

1. File input/output: [task](http://www.rosettacode.org/wiki/File_input/output) and [solution](source/File_input_output.act)\
![](images/File_input_output.png)

1. File size: [task](http://www.rosettacode.org/wiki/File_size) and [solution](source/File_size.act)\
![](images/File_size.png)

1. File size distribution: [task](http://www.rosettacode.org/wiki/File_size_distribution) and [solution](source/File_size_distribution.act)\
![](images/File_size_distribution.png)

1. Filter: [task](http://www.rosettacode.org/wiki/Filter) and [solution](source/Filter.act)\
![](images/Filter.png)

1. Find first missing positive: [task](http://www.rosettacode.org/wiki/Find_first_missing_positive) and [solution](source/Find_first_missing_positive.act)\
![](images/Find_first_missing_positive.png)

1. Find minimum number of coins that make a given value: [task](http://www.rosettacode.org/wiki/Find_minimum_number_of_coins_that_make_a_given_value) and [solution](source/Find_minimum_number_of_coins_that_make_a_given_value.act)\
![](images/Find_minimum_number_of_coins_that_make_a_given_value.png)

1. Find the missing permutation: [task](http://www.rosettacode.org/wiki/Find_the_missing_permutation) and [solution](source/Find_the_missing_permutation.act)\
![](images/Find_the_missing_permutation.png)

1. Find prime n such that reversed n is also prime: [task](http://www.rosettacode.org/wiki/Find_prime_n_such_that_reversed_n_is_also_prime) and [solution](source/Find_prime_n_such_that_reversed_n_is_also_prime.act)\
![](images/Find_prime_n_such_that_reversed_n_is_also_prime.png)

1. Find the intersection of a line with a plane: [task](http://www.rosettacode.org/wiki/Find_the_intersection_of_a_line_with_a_plane) and [solution](source/Find_the_intersection_of_a_line_with_a_plane.act)\
![](images/Find_the_intersection_of_a_line_with_a_plane.png)

1. Find the intersection of two lines: [task](http://www.rosettacode.org/wiki/Find_the_intersection_of_two_lines) and [solution](source/Find_the_intersection_of_two_lines.act)\
![](images/Find_the_intersection_of_two_lines.png)

1. Find the last Sunday of each month: [task](http://www.rosettacode.org/wiki/Find_the_last_Sunday_of_each_month) and [solution](source/Find_the_last_Sunday_of_each_month.act)\
![](images/Find_the_last_Sunday_of_each_month.png)

1. Find words which contain the most consonants: [task](http://www.rosettacode.org/wiki/Find_words_which_contain_the_most_consonants) and [solution](source/Find_words_which_contain_the_most_consonants.act)\
![](images/Find_words_which_contain_the_most_consonants.png)

1. Find words which contains all the vowels: [task](http://www.rosettacode.org/wiki/Find_words_which_contains_all_the_vowels) and [solution](source/Find_words_which_contains_all_the_vowels.act)\
![](images/Find_words_which_contains_all_the_vowels.png)

1. Find words which contains more than 3 e vowels: [task](http://www.rosettacode.org/wiki/Find_words_which_contains_more_than_3_e_vowels) and [solution](source/Find_words_which_contains_more_than_3_e_vowels.act)\
![](images/Find_words_which_contains_more_than_3_e_vowels.png)

1. Find words whose first and last three letters are equal: [task](http://www.rosettacode.org/wiki/Find_words_whose_first_and_last_three_letters_are_equal) and [solution](source/Find_words_whose_first_and_last_three_letters_are_equal.act)\
![](images/Find_words_whose_first_and_last_three_letters_are_equal.png)

1. Find words with alternating vowels and consonants: [task](http://www.rosettacode.org/wiki/Find_words_with_alternating_vowels_and_consonants) and [solution](source/Find_words_with_alternating_vowels_and_consonants.act)\
![](images/Find_words_with_alternating_vowels_and_consonants.png)

1. Five weekends: [task](http://www.rosettacode.org/wiki/Five_weekends) and [solution](source/Five_weekends.act)\
![](images/Five_weekends.png)

1. FizzBuzz: [task](http://www.rosettacode.org/wiki/FizzBuzz) and [solution](source/FizzBuzz.act)\
![](images/FizzBuzz.png)

1. Flipping bits game: [task](http://www.rosettacode.org/wiki/Flipping_bits_game) and [solution](source/Flipping_bits_game.act)\
![](images/Flipping_bits_game.png)

1. Floyd's triangle: [task](http://www.rosettacode.org/wiki/Floyd%27s_triangle) and [solution](source/Floyd's_triangle.act)\
![](images/Floyd's_triangle.png)

1. Four bit adder: [task](http://www.rosettacode.org/wiki/Four_bit_adder) and [solution](source/Four_bit_adder.act)\
![](images/Four_bit_adder.png)

1. Fractal tree: [task](http://www.rosettacode.org/wiki/Fractal_tree) and [solution](source/Fractal_tree.act)\
![](images/Fractal_tree.png)

1. Frobenius numbers: [task](http://www.rosettacode.org/wiki/Frobenius_numbers) and [solution](source/Frobenius_numbers.act)\
![](images/Frobenius_numbers.png)

1. General FizzBuzz: [task](http://www.rosettacode.org/wiki/General_FizzBuzz) and [solution](source/General_FizzBuzz.act)\
![](images/General_FizzBuzz.png)

1. Generate Chess960 starting position: [task](http://www.rosettacode.org/wiki/Generate_Chess960_starting_position) and [solution](source\Generate_Chess960_starting_position_.act)\
![](images/Generate_Chess960_starting_position_.png)

1. Generate lower case ASCII alphabet: [task](http://www.rosettacode.org/wiki/Generate_lower_case_ASCII_alphabet) and [solution](source/Generate_lower_case_ASCII_alphabet.act)\
![](images/Generate_lower_case_ASCII_alphabet.png)

1. Generate random chess position: [task](http://www.rosettacode.org/wiki/Generate_random_chess_position) and [solution](source/Generate_random_chess_position.act)\
![](images/Generate_random_chess_position.png)

1. Generate random numbers without repeating a value: [task](http://www.rosettacode.org/wiki/Generate_random_numbers_without_repeating_a_value) and [solution](source/Generate_random_numbers_without_repeating_a_value.act)\
![](images/Generate_random_numbers_without_repeating_a_value.png)

1. Generic swap: [task](http://www.rosettacode.org/wiki/Generic_swap) and [solution](source/Generic_swap.act)\
![](images/Generic_swap.png)

1. Geohash: [task](http://www.rosettacode.org/wiki/Geohash) and [solution](source/Geohash.act)\
![](images/Geohash.png)

1. Getting the number of decimals: [task](http://www.rosettacode.org/wiki/Getting_the_number_of_decimals) and [solution](source/Getting_the_number_of_decimals.act)\
![](images/Getting_the_number_of_decimals.png)

1. Gray code: [task](http://www.rosettacode.org/wiki/Gray_code) and [solution](source/Gray_code.act)\
![](images/Gray_code.png)

1. Grayscale image: [task](http://www.rosettacode.org/wiki/Grayscale_image) and [solution1](source/Grayscale_image.act) [solution2](source/GrImage.act) [solution3](source/Rgb2Gray.act)\
![](images/Grayscale_image.png)

1. Greatest common divisor: [task](http://www.rosettacode.org/wiki/Greatest_common_divisor) and [solution](source/Greatest_common_divisor.act)\
![](images/Greatest_common_divisor.png)

1. Greatest element of a list: [task](http://www.rosettacode.org/wiki/Greatest_element_of_a_list) and [solution](source/Greatest_element_of_a_list.act)\
![](images/Greatest_element_of_a_list.png)

1. Greatest subsequential sum: [task](http://www.rosettacode.org/wiki/Greatest_subsequential_sum) and [solution](source/Greatest_subsequential_sum.act)\
![](images/Greatest_subsequential_sum.png)

1. Guess the number: [task](http://www.rosettacode.org/wiki/Guess_the_number) and [solution](source/Guess_the_number.act)\
![](images/Guess_the_number.png)

1. Guess the number/With feedback: [task](http://www.rosettacode.org/wiki/Guess_the_number/With_feedback) and [solution](source/Guess_the_number_with_feedback.act)\
![](images/Guess_the_number_with_feedback.png)

1. Guess the number/With feedback (player): [task](http://www.rosettacode.org/wiki/Guess_the_number/With_feedback_(player)) and [solution](source/Guess_the_number_with_feedback_(player).act)\
![](images/Guess_the_number_with_feedback_(player).png)

1. Happy numbers: [task](http://www.rosettacode.org/wiki/Happy_numbers) and [solution](source/Happy_numbers.act)\
![](images/Happy_numbers.png)

1. Harshad or Niven series: [task](http://www.rosettacode.org/wiki/Harshad_or_Niven_series) and [solution](source/Harshad_or_Niven_series.act)\
![](images/Harshad_or_Niven_series.png)

1. Hello world/Graphical: [task](http://www.rosettacode.org/wiki/Hello_world/Graphical) and [solution](source/Hello_world_graphical.act)\
![](images/Hello_world_graphical.png)

1. Hello world/Line printer: [task](http://www.rosettacode.org/wiki/Hello_world/Line_printer) and [solution](source/Hello_world_line_printer.act)\
![](images/Hello_world_line_printer.png)

1. Hello world/Newline omission: [task](http://www.rosettacode.org/wiki/Hello_world/Newline_omission) and [solution](source/Newline_omission.act)\
![](images/Newline_omission.png)

1. Hello world/Text: [task](http://www.rosettacode.org/wiki/Hello_world/Text) and [solution](source/Hello_world.act)\
![](images/Hello_world.png)

1. Hilbert curve: [task](http://www.rosettacode.org/wiki/Hilbert_curve) and [solution](source/Hilbert_curve.act)\
![](images/Hilbert_curve.png)

1. Hofstadter Q sequence: [task](http://www.rosettacode.org/wiki/Hofstadter_Q_sequence) and [solution](source/Hofstadter_Q_sequence.act)\
![](images/Hofstadter_Q_sequence.png)

1. Horner's rule for polynomial evaluation: [task](http://www.rosettacode.org/wiki/Horner%27s_rule_for_polynomial_evaluation) and [solution](source/Horner's_rule_for_polynomial_evaluation.act)\
![](images/Horner's_rule_for_polynomial_evaluation.png)

1. Host introspection: [task](http://www.rosettacode.org/wiki/Host_introspection) and [solution](source/Host_introspection.act)\
![](images/Host_introspection.png)

1. Identity matrix: [task](http://www.rosettacode.org/wiki/Identity_matrix) and [solution](source/Identity_matrix.act)\
![](images/Identity_matrix.png)

1. Idiomatically determine all the lowercase and uppercase letters: [task](http://www.rosettacode.org/wiki/Idiomatically_determine_all_the_lowercase_and_uppercase_letters) and [solution](source/Idiomatically_determine_all_the_lowercase_and_uppercase_letters.act)\
![](images/Idiomatically_determine_all_the_lowercase_and_uppercase_letters.png)

1. Image convolution: [task](http://www.rosettacode.org/wiki/Image_convolution) and [solution](source/Image_convolution.act)\
![](images/Image_convolution.png)

1. Image noise: [task](http://www.rosettacode.org/wiki/Image_noise) and [solution](source/Image_noise.act)\
![](images/Image_noise.png)

1. Include a file: [task](http://www.rosettacode.org/wiki/Include_a_file) and [solution](source/Include_a_file.act)\
![](images/Include_a_file.png)

1. Increment a numerical string: [task](http://www.rosettacode.org/wiki/Increment_a_numerical_string) and [solution](source/Increment_a_numerical_string.act)\
![](images/Increment_a_numerical_string.png)

1. Input loop: [task](http://www.rosettacode.org/wiki/Input_loop) and [solution](source/Input_loop.act)\
![](images/Input_loop.png)

1. Input/Output for lines of text: [task](http://www.rosettacode.org/wiki/Input/Output_for_lines_of_text) and [solution](source/Input_Output_for_lines_of_text.act)\
![](images/Input_Output_for_lines_of_text.png)

1. Input/Output for pairs of numbers: [task](http://www.rosettacode.org/wiki/Input/Output_for_pairs_of_numbers) and [solution](source/Input_Output_for_pairs_of_numbers.act)\
![](images/Input_Output_for_pairs_of_numbers.png)

1. Integer comparison: [task](http://www.rosettacode.org/wiki/Integer_comparison) and [solution](source/Integer_comparison.act)\
![](images/Integer_comparison.png)

1. Integer sequence: [task](http://www.rosettacode.org/wiki/Integer_sequence) and [solution](source/Integer_sequence.act)\
![](images/Integer_sequence.png)

1. ISBN13 check digit: [task](http://www.rosettacode.org/wiki/ISBN13_check_digit) and [solution](source/ISBN13_check_digit.act)\
![](images/ISBN13_check_digit.png)

1. Jacobi symbol: [task](http://www.rosettacode.org/wiki/Jacobi_symbol) and [solution](source/Jacobi_symbol.act)\
![](images/Jacobi_symbol.png)

1. JortSort: [task](http://www.rosettacode.org/wiki/JortSort) and [solution](source/JortSort.act)\
![](images/JortSort.png)

1. Joystick position: [task](http://www.rosettacode.org/wiki/Joystick_position) and [solution](source/Joystick_position.act)\
![](images/Joystick_position.png)

1. Kernighans large earthquake problem: [task](http://www.rosettacode.org/wiki/Kernighans_large_earthquake_problem) and [solution](source/Kernighans_large_earthquake_problem.act)\
![](images/Kernighans_large_earthquake_problem.png)

1. Keyboard input/Flush the keyboard buffer: [task](http://www.rosettacode.org/wiki/Keyboard_input/Flush_the_keyboard_buffer) and [solution](source/Flush_the_keyboard_buffer.act)\
![](images/Flush_the_keyboard_buffer.png)

1. Keyboard input/Keypress check: [task](http://www.rosettacode.org/wiki/Keyboard_input/Keypress_check) and [solution](source/Keypress_check.act)\
![](images/Keypress_check.png)

1. Keyboard input/Obtain a Y or N response: [task](http://www.rosettacode.org/wiki/Keyboard_input/Obtain_a_Y_or_N_response) and [solution](source/Obtain_a_Y_or_N_response.act)\
![](images/Obtain_a_Y_or_N_response.png)

1. Knuth shuffle: [task](http://www.rosettacode.org/wiki/Knuth_shuffle) and [solution](source/Knuth_shuffle.act)\
![](images/Knuth_shuffle.png)

1. Koch curve: [task](http://www.rosettacode.org/wiki/Koch_curve) and [solution](source/Koch_curve.act)\
![](images/Koch_curve.png)

1. Kronecker product: [task](http://www.rosettacode.org/wiki/Kronecker_product) and [solution](source/Kronecker_product.act)\
![](images/Kronecker_product.png)

1. Kronecker product based fractals: [task](http://www.rosettacode.org/wiki/Kronecker_product_based_fractals) and [solution](source/Kronecker_product_based_fractals.act)\
![](images/Kronecker_product_based_fractals.png)

1. Langton's ant: [task](http://www.rosettacode.org/wiki/Langton%27s_ant) and [solution](source/Langton's_ant.act)\
![](images/Langton's_ant.png)

1. Last Friday of each month: [task](http://www.rosettacode.org/wiki/Last_Friday_of_each_month) and [solution](source/Last_Friday_of_each_month.act)\
![](images/Last_Friday_of_each_month.png)

1. Law of cosines - triples: [task](http://www.rosettacode.org/wiki/Law_of_cosines_-_triples) and [solution](source/Law_of_cosines_-_triples.act)\
![](images/Law_of_cosines_-_triples.png)

1. Leap year: [task](http://www.rosettacode.org/wiki/Leap_year) and [solution](source/Leap_year.act)\
![](images/Leap_year.png)

1. Least common multiple: [task](https://rosettacode.org/wiki/Least_common_multiple) and [solution](source/Least_common_multiple.act)\
![](images/Least_common_multiple.png)

1. Length of an arc between two angles: [task](http://www.rosettacode.org/wiki/Length_of_an_arc_between_two_angles) and [solution](source/Length_of_an_arc_between_two_angles.act)\
![](images/Length_of_an_arc_between_two_angles.png)

1. Leonardo numbers: [task](http://www.rosettacode.org/wiki/Leonardo_numbers) and [solution](source/Leonardo_numbers.act)\
![](images/Leonardo_numbers.png)

1. Letter frequency: [task](http://www.rosettacode.org/wiki/Letter_frequency) and [solution](source/Letter_frequency.act)\
![](images/Letter_frequency.png)

1. Logical operations: [task](http://www.rosettacode.org/wiki/Logical_operations) and [solution](source/Logical_operations.act)\
![](images/Logical_operations.png)

1. Long year: [task](http://www.rosettacode.org/wiki/Long_year) and [solution](source/Long_year.act)\
![](images/Long_year.png)

1. Longest common prefix: [task](http://www.rosettacode.org/wiki/Longest_common_prefix) and [solution](source/Longest_common_prefix.act)\
![](images/Longest_common_prefix.png)

1. Longest common substring: [task](http://www.rosettacode.org/wiki/Longest_common_substring) and [solution](source/Longest_common_substring.act)\
![](images/Longest_common_substring.png)

1. Longest common suffix: [task](http://www.rosettacode.org/wiki/Longest_common_suffix) and [solution](source/Longest_common_suffix.act)\
![](images/Longest_common_suffix.png)

1. Longest palindromic substrings: [task](http://www.rosettacode.org/wiki/Longest_palindromic_substrings) and [solution](source/Longest_palindromic_substrings.act)\
![](images/Longest_palindromic_substrings.png)

1. Longest substrings without repeating characters: [task](http://www.rosettacode.org/wiki/Longest_substrings_without_repeating_characters) and [solution](source/Longest_substrings_without_repeating_characters.act)\
![](images/Longest_substrings_without_repeating_characters.png)

1. Look-and-say sequence: [task](http://www.rosettacode.org/wiki/Look-and-say_sequence) and [solution](source/Look-and-say_sequence.act)\
![](images/Look-and-say_sequence.png)

1. Loop over multiple arrays simultaneously: [task](http://www.rosettacode.org/wiki/Loop_over_multiple_arrays_simultaneously) and [solution](source/Loop_over_multiple_arrays_simultaneously.act)\
![](images/Loop_over_multiple_arrays_simultaneously.png)

1. Loops/Break: [task](www.rosettacode.org/wiki/Loops/Break) and [solution](source/Break.act)\
![](images/Break.png)

1. Loops/Do-while: [task](http://www.rosettacode.org/wiki/Loops/Do-while) and [solution](source/Do-while.act)\
![](images/Do-while.png)

1. Loops/For: [task](http://www.rosettacode.org/wiki/Loops/For) and [solution](source/For.act)\
![](images/For.png)

1. Loops/For with a specified step: [task](http://www.rosettacode.org/wiki/Loops/For_with_a_specified_step) and [solution](source/For_with_a_specified_step.act)\
![](images/For_with_a_specified_step.png)

1. Loops/Foreach: [task](http://www.rosettacode.org/wiki/Loops/Foreach) and [solution](source/Foreach.act)\
![](images/Foreach.png)

1. Loops/Infinite: [task](http://www.rosettacode.org/wiki/Loops/Infinite) and [solution](source/Infinite.act)\
![](images/Infinite.png)

1. Loops/N plus one half: [task](http://www.rosettacode.org/wiki/Loops/N_plus_one_half) and [solution](source/N_plus_one_half.act)\
![](images/N_plus_one_half.png)

1. Loops/Nested: [task](http://www.rosettacode.org/wiki/Loops/Nested) and [solution](source/Loops_nested.act)\
![](images/Loops_nested.png)

1. Loops/While: [task](http://www.rosettacode.org/wiki/Loops/While) and [solution](source/While.act)\
![](images/While.png)

1. Ludic numbers: [task](http://www.rosettacode.org/wiki/Ludic_numbers) and [solution](source/Ludic_numbers.act)\
![](images/Ludic_numbers.png)

1. Luhn test of credit card numbers: [task](http://www.rosettacode.org/wiki/Luhn_test_of_credit_card_numbers) and [solution](source/Luhn_test_of_credit_card_numbers.act)\
![](images/Luhn_test_of_credit_card_numbers.png)

1. Machine code: [task](http://www.rosettacode.org/wiki/Machine_code) and [solution](source/Machine_code.act)\
![](images/Machine_code.png)

1. Magic_8-ball: [task](http://www.rosettacode.org/wiki/Magic_8-ball) and [solution](source/Magic_8-ball.act)\
![](images/Magic_8-ball.png)

1. Map range: [task](http://www.rosettacode.org/wiki/Map_range) and [solution](source/Map_range.act)\
![](images/Map_range.png)

1. Mastermind: [task](http://www.rosettacode.org/wiki/Mastermind) and [solution](source/Mastermind.act)\
![](images/Mastermind.png)

1. Matrix multiplication: [task](http://www.rosettacode.org/wiki/Matrix_multiplication) and [solution](source/Matrix_multiplication.act)\
![](images/Matrix_multiplication.png)

1. Matrix transposition: [task](http://www.rosettacode.org/wiki/Matrix_transposition) and [solution](source/Matrix_transposition.act)\
![](images/Matrix_transposition.png)

1. Maximum difference between adjacent elements of list: [task](http://www.rosettacode.org/wiki/Maximum_difference_between_adjacent_elements_of_list) and [solution](source/Maximum_difference_between_adjacent_elements_of_list.act)\
![](images/Maximum_difference_between_adjacent_elements_of_list.png)

1. Maximum triangle path sum: [task](http://www.rosettacode.org/wiki/Maximum_triangle_path_sum) and [solution](source/Maximum_triangle_path_sum.act)\
![](images/Maximum_triangle_path_sum.png)

1. Maze generation: [task](http://www.rosettacode.org/wiki/Maze_generation) and [solution](source/Maze_generation.act)\
![](images/Maze_generation.png)

1. Maze solving: [task](http://www.rosettacode.org/wiki/Maze_solving) and [solution](source/Maze_solving.act)\
![](images/Maze_solving.png)

1. Median filter: [task](http://www.rosettacode.org/wiki/Median_filter) and [solution](source/Median_filter.act)\
![](images/Median_filter.png)

1. Memory allocation: [task](http://www.rosettacode.org/wiki/Memory_allocation) and [solution](source/Memory_allocation.act)\
![](images/Memory_allocation.png)

1. Menu: [task](http://www.rosettacode.org/wiki/Menu) and [solution](source/Menu.act)\
![](images/Menu.png)

1. Mertens function: [task](http://www.rosettacode.org/wiki/Mertens_function) and [solution](source/Mertens_function.act)\
![](images/Mertens_function.png)

1. McNuggets problem: [task](http://www.rosettacode.org/wiki/McNuggets_problem) and [solution](source/McNuggets_problem.act)\
![](images/McNuggets_problem.png)

1. Middle three digits: [task](http://www.rosettacode.org/wiki/Middle_three_digits) and [solution](source/Middle_three_digits.act)\
![](images/Middle_three_digits.png)

1. Modular inverse: [task](http://www.rosettacode.org/wiki/Modular_inverse) and [solution](source/Modular_inverse.act)\
![](images/Modular_inverse.png)

1. Monte Carlo methods: [task](http://www.rosettacode.org/wiki/Monte_Carlo_methods) and [solution](source/Monte_Carlo_methods.act)\
![](images/Monte_Carlo_methods.png)

1. Morse code: [task](http://www.rosettacode.org/wiki/Morse_code) and [solution](source/Morse_code.act)\
![](images/Morse_code.png)

1. Move-to-front algorithm: [task](http://www.rosettacode.org/wiki/Move-to-front_algorithm) and [solution](source/Move-to-front_algorithm.act)\
![](images/Move-to-front_algorithm.png)

1. Multifactorial: [task](http://www.rosettacode.org/wiki/Multifactorial) and [solution](source/Multifactorial.act)\
![](images/Multifactorial.png)

1. Multiple distinct objects: [task](http://www.rosettacode.org/wiki/Multiple_distinct_objects) and [solution](source/Multiple_distinct_objects.act)\
![](images/Multiple_distinct_objects.png)

1. Multiplication tables: [task](http://www.rosettacode.org/wiki/Multiplication_tables) and [solution](source/Multiplication_tables.act)\
![](images/Multiplication_tables.png)

1. Munchausen numbers: [task](http://www.rosettacode.org/wiki/Munchausen_numbers) and [solution](source/Munchausen_numbers.act)\
![](images/Munchausen_numbers.png)

1. Munching squares: [task](http://www.rosettacode.org/wiki/Munching_squares) and [solution](source/Munching_squares.act)\
![](images/Munching_squares.png)

1. Musical scale: [task](http://www.rosettacode.org/wiki/Musical_scale) and [solution](source/Musical_scale.act)\
![](images/Musical_scale.png)

1. N'th: [task](http://www.rosettacode.org/wiki/N%27th) and [solution](source/N'th.act)\
![](images/N'th.png)

1. Negative base numbers: [task](http://www.rosettacode.org/wiki/Negative_base_numbers) and [solution](source/Negative_base_numbers.act)\
![](images/Negative_base_numbers.png)

1. Next special primes: [task](http://www.rosettacode.org/wiki/Next_special_primes) and [solution](source/Next_special_primes.act)\
![](images/Next_special_primes.png)

1. Nice primes: [task](http://www.rosettacode.org/wiki/Nice_primes) and [solution](source/Nice_primes.act)\
![](images/Nice_primes.png)

1. Nim game: [task](http://www.rosettacode.org/wiki/Nim_game) and [solution](source/Nim_game.act)\
![](images/Nim_game.png)

1. Non-decimal radices/Convert: [task](http://www.rosettacode.org/wiki/Non-decimal_radices/Convert) and [solution](source/Non-decimal_radices_convert.act)\
![](images/Non-decimal_radices_convert.png)

1. Non-decimal radices/Output: [task](http://www.rosettacode.org/wiki/Non-decimal_radices/Output) and [solution](source/Non-decimal_radices_output.act)\
![](images/Non-decimal_radices_output.png)

1. Nonoblock: [task](http://www.rosettacode.org/wiki/Nonoblock) and [solution](source/Nonoblock.act)\
![](images/Nonoblock.png)

1. Nth root: [task](http://www.rosettacode.org/wiki/Nth_root) and [solution](source/Nth_root.act)\
![](images/Nth_root.png)

1. Null object: [task](http://www.rosettacode.org/wiki/Null_object) and [solution](source/Null_object.act)\
![](images/Null_object.png)

1. Number reversal game: [task](http://www.rosettacode.org/wiki/Number_reversal_game) and [solution](source/Number_reversal_game.act)\
![](images/Number_reversal_game.png)

1. Numbers divisible by their individual digits, but not by the product of their digits: [task](http://www.rosettacode.org/wiki/Numbers_divisible_by_their_individual_digits,_but_not_by_the_product_of_their_digits.) and [solution](source/Numbers_divisible_by_their_individual_digits,_but_not_by_the_product_of_their_digits.act)\
![](images/Numbers_divisible_by_their_individual_digits,_but_not_by_the_product_of_their_digits.png)

1. Numbers in base 10 that are palindromic in bases 2, 4, and 16: [task](http://www.rosettacode.org/wiki/Numbers_in_base_10_that_are_palindromic_in_bases_2,_4,_and_16) and [solution](source/Numbers_in_base_10_that_are_palindromic_in_bases_2,_4,_and_16.act)\
![](images/Numbers_in_base_10_that_are_palindromic_in_bases_2,_4,_and_16.png)

1. Numbers in base-16 representation that cannot be written with decimal digits: [task](http://www.rosettacode.org/wiki/Numbers_in_base-16_representation_that_cannot_be_written_with_decimal_digits) and [solution](source/Numbers_in_base-16_representation_that_cannot_be_written_with_decimal_digits.act)\
![](images/Numbers_in_base-16_representation_that_cannot_be_written_with_decimal_digits.png)

1. Numbers which binary and ternary digit sum are prime: [task](http://www.rosettacode.org/wiki/Numbers_which_binary_and_ternary_digit_sum_are_prime) and [solution](source/Numbers_which_binary_and_ternary_digit_sum_are_prime.act)\
![](images/Numbers_which_binary_and_ternary_digit_sum_are_prime.png)

1. Numbers whose count of divisors is prime: [task](http://www.rosettacode.org/wiki/Numbers_whose_count_of_divisors_is_prime) and [solution](source/Numbers_whose_count_of_divisors_is_prime.act)\
![](images/Numbers_whose_count_of_divisors_is_prime.png)

1. Numbers with prime digits whose sum is 13: [task](http://www.rosettacode.org/wiki/Numbers_with_prime_digits_whose_sum_is_13) and [solution](source/Numbers_with_prime_digits_whose_sum_is_13.act)\
![](images/Numbers_with_prime_digits_whose_sum_is_13.png)

1. Odd squarefree semiprimes: [task](http://www.rosettacode.org/wiki/Odd_squarefree_semiprimes) and [solution](source/Odd_squarefree_semiprimes.act)\
![](images/Odd_squarefree_semiprimes.png)

1. Old Russian measure of length: [task](http://www.rosettacode.org/wiki/Old_Russian_measure_of_length) and [solution](source/Old_Russian_measure_of_length.act)\
![](images/Old_Russian_measure_of_length.png)

1. One-dimensional cellular automata: [task](http://www.rosettacode.org/wiki/One-dimensional_cellular_automata) and [solution](source/One-dimensional_cellular_automata.act)\
![](images/One-dimensional_cellular_automata.png)

1. Order by pair comparisons: [task](http://www.rosettacode.org/wiki/Order_by_pair_comparisons) and [solution](source/Order_by_pair_comparisons.act)\
![](images/Order_by_pair_comparisons.png)

1. Order two numerical lists: [task](http://www.rosettacode.org/wiki/Order_two_numerical_lists) and [solution](source/Order_two_numerical_lists.act)\
![](images/Order_two_numerical_lists.png)

1. Ordered words: [task](http://www.rosettacode.org/wiki/Ordered_words) and [solution](source/Ordered_words.act)\
![](images/Ordered_words.png)

1. Palindrome dates: [task](http://www.rosettacode.org/wiki/Palindrome_dates) and [solution](source/Palindrome_dates.act)\
![](images/Palindrome_dates.png)

1. Palindrome detection: [task](http://www.rosettacode.org/wiki/Palindrome_detection) and [solution](source/Palindrome_detection.act)\
![](images/Palindrome_detection.png)

1. Palindromic primes: [task](http://www.rosettacode.org/wiki/Palindromic_primes) and [solution](source/Palindromic_primes.act)\
![](images/Palindromic_primes.png)

1. Palindromic primes in base 16: [task](http://www.rosettacode.org/wiki/Palindromic_primes_in_base_16) and [solution](source/Palindromic_primes_in_base_16.act)\
![](images/Palindromic_primes_in_base_16.png)

1. Pangram checker: [task](http://www.rosettacode.org/wiki/Pangram_checker) and [solution](source/Pangram_checker.act)\
![](images/Pangram_checker.png)

1. Parsing/RPN calculator algorithm: [task](http://www.rosettacode.org/wiki/Parsing/RPN_calculator_algorithm) and [solution](source/RPN_calculator_algorithm.act)\
![](images/RPN_calculator_algorithm.png)

1. Pascal matrix generation: [task](http://www.rosettacode.org/wiki/Pascal_matrix_generation) and [solution](source/Pascal_matrix_generation.act)\
![](images/Pascal_matrix_generation.png)

1. Pascal's triangle: [task](http://www.rosettacode.org/wiki/Pascal%27s_triangle) and [solution](source/Pascal's_triangle.act)\
![](images/Pascal's_triangle.png)

1. Password generator: [task](http://www.rosettacode.org/wiki/Password_generator) and [solution](source/Password_generator.act)\
![](images/Password_generator.png)

1. Peano curve: [task](http://www.rosettacode.org/wiki/Peano_curve) and [solution](source/Peano_curve.act)\
![](images/Peano_curve.png)

1. Pentagram: [task](http://www.rosettacode.org/wiki/Pentagram) and [solution](source/Pentagram.act)\
![](images/Pentagram.png)

1. Perfect numbers: [task](http://www.rosettacode.org/wiki/Perfect_numbers) and [solution](source/Perfect_numbers.act)\
![](images/Perfect_numbers.png)

1. Perfect shuffle: [task](http://www.rosettacode.org/wiki/Perfect_shuffle) and [solution](source/Perfect_shuffle.act)\
![](images/Perfect_shuffle.png)

1. Peripheral drift illusion: [task](http://www.rosettacode.org/wiki/Peripheral_drift_illusion) and [solution](source/Peripheral_drift_illusion.act)\
![](images/Peripheral_drift_illusion.png)

1. Permutations: [task](http://www.rosettacode.org/wiki/Permutations) and [solution](source/Permutations.act)\
![](images/Permutations.png)

1. Phrase reversals: [task](http://www.rosettacode.org/wiki/Phrase_reversals) and [solution](source/Phrase_reversals.act)\
![](images/Phrase_reversals.png)

1. Pick random element: [task](http://www.rosettacode.org/wiki/Pick_random_element) and [solution](source/Pick_random_element.act)\
![](images/Pick_random_element.png)

1. Pinstripe/Display: [task](http://www.rosettacode.org/wiki/Pinstripe/Display) and [solution](source/Pinstripe_display.act)\
![](images/Pinstripe_display.png)

1. Piprimes: [task](http://www.rosettacode.org/wiki/Piprimes) and [solution](source/Piprimes.act)\
![](images/Piprimes.png)

1. Playing cards: [task](http://www.rosettacode.org/wiki/Playing_cards) and [solution](source/Playing_cards.act)\
![](images/Playing_cards.png)

1. Plot coordinate pairs: [task](http://www.rosettacode.org/wiki/Plot_coordinate_pairs) and [solution](source/Plot_coordinate_pairs.act)\
![](images/Plot_coordinate_pairs.png)

1. Polyspiral: [task](http://www.rosettacode.org/wiki/Polyspiral) and [solution](source/Polyspiral.act)\
![](images/Polyspiral.png)

1. Positive decimal integers with the digit 1 occurring exactly twice: [task](http://www.rosettacode.org/wiki/Positive_decimal_integers_with_the_digit_1_occurring_exactly_twice) and [solution](source/Positive_decimal_integers_with_the_digit_1_occurring_exactly_twice.act)\
![](images/Positive_decimal_integers_with_the_digit_1_occurring_exactly_twice.png)

1. Price fraction: [task](http://www.rosettacode.org/wiki/Price_fraction) and [solution](source/Price_fraction.act)\
![](images/Price_fraction.png)

1. Primality by trial division: [task](https://rosettacode.org/wiki/Primality_by_trial_division) and [solution](source/Primality_by_trial_division.act)\
![](images/Primality_by_trial_division.png)

1. Prime triplets: [task](http://www.rosettacode.org/wiki/Prime_triplets) and [solution](source/Prime_triplets.act)\
![](images/Prime_triplets.png)

1. Prime words: [task](http://www.rosettacode.org/wiki/Prime_words) and [solution](source/Prime_words.act)\
![](images/Prime_words.png)

1. Primes which contain only one odd digit: [task](http://www.rosettacode.org/wiki/Primes_which_contain_only_one_odd_digit) and [solution](source/Primes_which_contain_only_one_odd_digit.act)\
![](images/Primes_which_contain_only_one_odd_digit.png)

1. Primes which sum of digits is 25: [task](http://www.rosettacode.org/wiki/Primes_which_sum_of_digits_is_25) and [solution](source/Primes_which_sum_of_digits_is_25.act)\
![](images/Primes_which_sum_of_digits_is_25.png)

1. Primes whose first and last number is 3: [task](http://www.rosettacode.org/wiki/Primes_whose_first_and_last_number_is_3) and [solution](source/Primes_whose_first_and_last_number_is_3.act)\
![](images/Primes_whose_first_and_last_number_is_3.png)

1. Primes with digits in nondecreasing order: [task](http://www.rosettacode.org/wiki/Primes_with_digits_in_nondecreasing_order) and [solution](source/Primes_with_digits_in_nondecreasing_order.act)\
![](images/Primes_with_digits_in_nondecreasing_order.png)

1. Priority queue: [task](http://www.rosettacode.org/wiki/Priority_queue) and [solution](source/Priority_queue.act)\
![](images/Priority_queue.png)

1. Product of divisors: [task](http://www.rosettacode.org/wiki/Product_of_divisors) and [solution](source/Product_of_divisors.act)\
![](images/Product_of_divisors.png)

1. Program termination: [task](http://www.rosettacode.org/wiki/Program_termination) and [solution](source/Program_termination.act)\
![](images/Program_termination.png)

1. Proper divisors: [task](http://www.rosettacode.org/wiki/Proper_divisors) and [solution](source/Proper_divisors.act)\
![](images/Proper_divisors.png)

1. Pythagorean triples: [task](http://www.rosettacode.org/wiki/Pythagorean_triples) and [solution](source/Pythagorean_triples.act)\
![](images/Pythagorean_triples.png)

1. Quadrat Special Primes: [task](http://www.rosettacode.org/wiki/Quadrat_Special_Primes) and [solution](source/Quadrat_Special_Primes.act)\
![](images/Quadrat_Special_Primes.png)

1. Quaternion type: [task](http://www.rosettacode.org/wiki/Quaternion_type) and [solution](source/Quaternion_type.act)\
![](images/Quaternion_type.png)

1. Queue/Definition: [task](http://www.rosettacode.org/wiki/Queue/Definition) and [solution1](source/Queue_array.act) [solution2](source/Queue_dynamic.act)\
![](images/Queue_array.png)

1. Queue/Usage: [task](http://www.rosettacode.org/wiki/Queue/Usage) and [solution](source/Queue_Usage.act)\
![](images/Queue_Usage.png)

1. Quickselect algorithm: [task](http://www.rosettacode.org/wiki/Quickselect_algorithm) and [solution](source/Quickselect_algorithm.act)\
![](images/Quickselect_algorithm.png)

1. Random Latin squares: [task](http://www.rosettacode.org/wiki/Random_Latin_squares) and [solution](source/Random_Latin_squares.act)\
![](images/Random_Latin_squares.png)

1. Range consolidation: [task](http://www.rosettacode.org/wiki/Range_consolidation) and [solution](source/Range_consolidation.act)\
![](images/Range_consolidation.png)

1. Range expansion: [task](http://www.rosettacode.org/wiki/Range_expansion) and [solution](source/Range_expansion.act)\
![](images/Range_expansion.png)

1. Range extraction: [task](http://www.rosettacode.org/wiki/Range_extraction) and [solution](source/Range_extraction.act)\
![](images/Range_extraction.png)

1. Range modifications: [task](http://www.rosettacode.org/wiki/Range_modifications) and [solution](source/Range_modifications.act)\
![](images/Range_modifications.png)

1. Rate counter: [task](http://www.rosettacode.org/wiki/Rate_counter) and [solution](source/Rate_counter.act)\
![](images/Rate_counter.png)

1. Read a file line by line: [task](http://www.rosettacode.org/wiki/Read_a_file_line_by_line) and [solution](source/Read_a_file_line_by_line.act)\
![](images/Read_a_file_line_by_line.png)

1. Read a specific line from a file: [task](http://www.rosettacode.org/wiki/Read_a_specific_line_from_a_file) and [solution](source/Read_a_specific_line_from_a_file.act)\
![](images/Read_a_specific_line_from_a_file.png)

1. Read entire file: [task](http://www.rosettacode.org/wiki/Read_entire_file) and [solution](source/Read_entire_file.act)\
![](images/Read_entire_file.png)

1. Real constants and functions: [task](http://www.rosettacode.org/wiki/Real_constants_and_functions) and [solution](source/Real_constants_and_functions.act)\
![](images/Real_constants_and_functions.png)

1. Remove duplicate elements: [task](http://www.rosettacode.org/wiki/Remove_duplicate_elements) and [solution](source/Remove_duplicate_elements.act)\
![](images/Remove_duplicate_elements.png)

1. Remove vowels from a string: [task](http://www.rosettacode.org/wiki/Remove_vowels_from_a_string) and [solution](source/Remove_vowels_from_a_string.act)\
![](images/Remove_vowels_from_a_string.png)

1. Rename a file: [task](http://www.rosettacode.org/wiki/Rename_a_file) and [solution](source/Rename_a_file.act)\
![](images/Rename_a_file.png)

1. Rep-string: [task](http://www.rosettacode.org/wiki/Rep-string) and [solution](source/Rep-string.act)\
![](images/Rep-string.png)

1. Repeat: [task](http://www.rosettacode.org/wiki/Repeat) and [solution](source/Repeat.act)\
![](images/Repeat.png)

1. Repeat a string: [task](http://www.rosettacode.org/wiki/Repeat_a_string) and [solution](source/Repeat_a_string.act)\
![](images/Repeat_a_string.png)

1. Return multiple values: [task](http://www.rosettacode.org/wiki/Return_multiple_values) and [solution](source/Return_multiple_values.act)\
![](images/Return_multiple_values.png)

1. Reverse a string: [task](http://www.rosettacode.org/wiki/Reverse_a_string) and [solution](source/Reverse_a_string.act)\
![](images/Reverse_a_string.png)

1. Reverse the gender of a string: [task](http://www.rosettacode.org/wiki/Reverse_the_gender_of_a_string) and [solution](source/Reverse_the_gender_of_a_string.act)\
![](images/Reverse_the_gender_of_a_string.png)

1. Reverse the order of lines in a text file while preserving the contents of each line: [task](http://www.rosettacode.org/wiki/Reverse_the_order_of_lines_in_a_text_file_while_preserving_the_contents_of_each_line) and [solution](source/Reverse_the_order_of_lines_in_a_text_file_while_preserving_the_contents_of_each_line.act)\
![](images/Reverse_the_order_of_lines_in_a_text_file_while_preserving_the_contents_of_each_line.png)

1. Reverse words in a string: [task](http://www.rosettacode.org/wiki/Reverse_words_in_a_string) and [solution](source/Reverse_words_in_a_string.act)\
![](images/Reverse_words_in_a_string.png)

1. Roman numerals/Decode: [task](http://www.rosettacode.org/wiki/Roman_numerals/Decode) and [solution](source/Roman_numerals_decode.act)\
![](images/Roman_numerals_decode.png)

1. Roman numerals/Encode: [task](http://www.rosettacode.org/wiki/Roman_numerals/Encode) and [solution](source/Roman_numerals_encode.act)\
![](images/Roman_numerals_encode.png)

1. RPG attributes generator: [task](http://www.rosettacode.org/wiki/RPG_attributes_generator) and [solution](source/RPG_attributes_generator.act)\
![](images/RPG_attributes_generator.png)

1. Run-length encoding: [task](http://www.rosettacode.org/wiki/Run-length_encoding) and [solution](source/Run-length_encoding.act)\
![](images/Run-length_encoding.png)

1. Runge-Kutta method: [task](http://www.rosettacode.org/wiki/Runge-Kutta_method) and [solution](source/Runge-Kutta_method.act)\
![](images/Runge-Kutta_method.png)

1. Sattolo cycle: [task](http://www.rosettacode.org/wiki/Sattolo_cycle) and [solution](source/Sattolo_cycle.act)\
![](images/Sattolo_cycle.png)

1. Search a list: [task](http://www.rosettacode.org/wiki/Search_a_list) and [solution](source/Search_a_list.act)\
![](images/Search_a_list.png)

1. Search a list of records: [task](http://www.rosettacode.org/wiki/Search_a_list_of_records) and [solution](source/Search_a_list_of_records.act)\
![](images/Search_a_list_of_records.png)

1. SEDOLs: [task](http://www.rosettacode.org/wiki/SEDOLs) and [solution](source/SEDOLs.act)\
![](images/SEDOLs.png)

1. Semiprime: [task](http://www.rosettacode.org/wiki/Semiprime) and [solution](source/Semiprime.act)\
![](images/Semiprime.png)

1. Sequence of primes by trial division: [task](http://www.rosettacode.org/wiki/Sequence_of_primes_by_trial_division) and [solution](source/Sequence_of_primes_by_trial_division.act)\
![](images/Sequence_of_primes_by_trial_division.png)

1. Sequence: smallest number greater than previous term with exactly n divisors: [task](http://www.rosettacode.org/wiki/Sequence:_smallest_number_greater_than_previous_term_with_exactly_n_divisors) and [solution](source/Sequence_smallest_number_greater_than_previous_term_with_exactly_n_divisors.act)\
![](images/Sequence_smallest_number_greater_than_previous_term_with_exactly_n_divisors.png)

1. Sequence: smallest number with exactly n divisors: [task](http://www.rosettacode.org/wiki/Sequence:_smallest_number_with_exactly_n_divisors) and [solution](source/Sequence_smallest_number_with_exactly_n_divisors.act)\
![](images/Sequence_smallest_number_with_exactly_n_divisors.png)

1. Set: [task](http://www.rosettacode.org/wiki/Set) and [solution](source/Set.act)\
![](images/Set.png)

1. Shift list elements to left by 3: [task](http://www.rosettacode.org/wiki/Shift_list_elements_to_left_by_3) and [solution](source/Shift_list_elements_to_left_by_3.act)\
![](images/Shift_list_elements_to_left_by_3.png)

1. Shoelace formula for polygonal area: [task](http://www.rosettacode.org/wiki/Shoelace_formula_for_polygonal_area) and [solution](source/Shoelace_formula_for_polygonal_area.act)\
![](images/Shoelace_formula_for_polygonal_area.png)

1. Short-circuit evaluation: [task](http://www.rosettacode.org/wiki/Short-circuit_evaluation) and [solution](source/Short-circuit_evaluation.act)\
![](images/Short-circuit_evaluation.png)

1. Show ASCII table: [task](http://www.rosettacode.org/wiki/Show_ASCII_table) and [solution](source/Show_ASCII_table.act)\
![](images/Show_ASCII_table.png)

1. Sierpinski carpet: [task](http://www.rosettacode.org/wiki/Sierpinski_carpet) and [solution](source/Sierpinski_carpet.act)\
![](images/Sierpinski_carpet.png)

1. Sierpinski curve: [task](http://www.rosettacode.org/wiki/Sierpinski_curve) and [solution](source/Sierpinski_curve.act)\
![](images/Sierpinski_curve.png)

1. Sierpinski pentagon: [task](http://www.rosettacode.org/wiki/Sierpinski_pentagon) and [solution](source/Sierpinski_pentagon.act)\
![](images/Sierpinski_pentagon.png)

1. Sierpinski triangle: [task](http://www.rosettacode.org/wiki/Sierpinski_triangle) and [solution](source/Sierpinski_triangle.act)\
![](images/Sierpinski_triangle.png)

1. Sierpinski triangle/Graphical: [task](http://www.rosettacode.org/wiki/Sierpinski_triangle/Graphical) and [solution](source/Sierpinski_triangle_graphical.act)\
![](images/Sierpinski_triangle_graphical.png)

1. Sieve of Eratosthenes: [task](http://www.rosettacode.org/wiki/Sieve_of_Eratosthenes) and [solution](source/Sieve_of_Eratosthenes.act)\
![](images/Sieve_of_Eratosthenes.png)

1. Simple turtle graphics: [task](http://www.rosettacode.org/wiki/Simple_turtle_graphics) and [solution](source/Simple_turtle_graphics.act)\
![](images/Simple_turtle_graphics.png)

1. Singly-linked list/Element definition: [task](http://www.rosettacode.org/wiki/Singly-linked_list/Element_definition) and [solution](source/Singly-linked_list_element_definition.act)\
![](images/Singly-linked_list_element_definition.png)

1. Singly-linked list/Element insertion: [task](http://www.rosettacode.org/wiki/Singly-linked_list/Element_insertion) and [solution](source/Singly-linked_list_element_insertion.act)\
![](images/Singly-linked_list_element_insertion.png)

1. Singly-linked list/Element removal: [task](http://www.rosettacode.org/wiki/Singly-linked_list/Element_removal) and [solution](source/Singly-linked_list_element_removal.act)\
![](images/Singly-linked_list_element_removal.png)

1. Singly-linked list/Traversal: [task](http://www.rosettacode.org/wiki/Singly-linked_list/Traversal) and [solution](source/Singly-linked_list_traversal.act)\
![](images/Singly-linked_list_traversal.png)

1. Sleep: [task](http://www.rosettacode.org/wiki/Sleep) and [solution](source/Sleep.act)\
![](images/Sleep.png)

1. Smallest square that begins with n: [task](http://www.rosettacode.org/wiki/Smallest_square_that_begins_with_n) and [solution](source/Smallest_square_that_begins_with_n.act)\
![](images/Smallest_square_that_begins_with_n.png)

1. Smarandache prime-digital sequence: [task](http://www.rosettacode.org/wiki/Smarandache_prime-digital_sequence) and [solution](source/Smarandache_prime-digital_sequence.act)\
![](images/Smarandache_prime-digital_sequence.png)

1. Smith numbers: [task](http://www.rosettacode.org/wiki/Smith_numbers) and [solution](source/Smith_numbers.act)\
![](images/Smith_numbers.png)

1. Sort a list of object identifiers: [task](http://www.rosettacode.org/wiki/Sort_a_list_of_object_identifiers) and [solution](source/Sort_a_list_of_object_identifiers.act)\
![](images/Sort_a_list_of_object_identifiers.png)

1. Sort an array of composite structures: [task](http://www.rosettacode.org/wiki/Sort_an_array_of_composite_structures) and [solution](source/Sort_an_array_of_composite_structures.act)\
![](images/Sort_an_array_of_composite_structures.png)

1. Sort an integer array: [task](http://www.rosettacode.org/wiki/Sort_an_integer_array) and [solution](source/Sort_an_integer_array.act)\
![](images/Sort_an_integer_array.png)

1. Sort disjoint sublist: [task](http://www.rosettacode.org/wiki/Sort_disjoint_sublist) and [solution](source/Sort_disjoint_sublist.act)\
![](images/Sort_disjoint_sublist.png)

1. Sort numbers lexicographically: [task](http://www.rosettacode.org/wiki/Sort_numbers_lexicographically) and [solution](source/Sort_numbers_lexicographically.act)\
![](images/Sort_numbers_lexicographically.png)

1. Sort the letters of string in alphabetical order: [task](http://www.rosettacode.org/wiki/Sort_the_letters_of_string_in_alphabetical_order) and [solution](source/Sort_the_letters_of_string_in_alphabetical_order.act)\
![](images/Sort_the_letters_of_string_in_alphabetical_order.png)

1. Sort using a custom comparator: [task](http://www.rosettacode.org/wiki/Sort_using_a_custom_comparator) and [solution](source/Sort_using_a_custom_comparator.act)\
![](images/Sort_using_a_custom_comparator.png)

1. Sorting algorithms/Bogosort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Bogosort) and [solution](source/Bogosort.act)\
![](images/Bogosort.png)

1. Sorting algorithms/Bubble sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Bubble_sort) and [solution](source/Bubble_sort.act)\
![](images/Bubble_sort.png)

1. Sorting algorithms/Cocktail sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Cocktail_sort) and [solution](source/Cocktail_Sort.act)\
![](images/Cocktail_Sort.png)

1. Sorting algorithms/Cocktail sort with shifting bounds: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Cocktail_sort_with_shifting_bounds) and [solution](source/Cocktail_sort_with_shifting_bounds.act)\
![](images/Cocktail_sort_with_shifting_bounds.png)

1. Sorting algorithms/Comb sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Comb_sort) and [solution](source/Comb_sort.act)\
![](images/Comb_sort.png)

1. Sorting algorithms/Counting sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Counting_sort) and [solution](source/Counting_sort.act)\
![](images/Counting_sort.png)

1. Sorting Algorithms/Circle Sort: [task](http://www.rosettacode.org/wiki/Sorting_Algorithms/Circle_Sort) and [solution](source/Circle_Sort.act)\
![](images/Circle_Sort.png)

1. Sorting algorithms/Cycle sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Cycle_sort) and [solution](source/Cycle_sort.act)\
![](images/Cycle_sort.png)

1. Sorting algorithms/Gnome sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Gnome_sort) and [solution](source/Gnome_sort.act)\
![](images/Gnome_sort.png)

1. Sorting algorithms/Insertion sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Insertion_sort) and [solution](source/Insertion_sort.act)\
![](images/Insertion_sort.png)

1. Sorting algorithms/Merge sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Merge_sort) and [solution](source/Merge_sort.act)\
![](images/Merge_sort.png)

1. Sorting algorithms/Pancake sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Pancake_sort) and [solution](source/Pancake_sort.act)\
![](images/Pancake_sort.png)

1. Sorting algorithms/Quicksort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Quicksort) and [solution](source/Quicksort.act)\
![](images/Quicksort.png)

1. Sorting algorithms/Selection sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Selection_sort) and [solution](source/Selection_sort.act)\
![](images/Selection_sort.png)

1. Sorting algorithms/Shell sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Shell_sort) and [solution](source/Shell_sort.act)\
![](images/Shell_sort.png)

1. Sorting algorithms/Stooge sort: [task](http://www.rosettacode.org/wiki/Sorting_algorithms/Stooge_sort) and [solution](source/Stooge_sort.act)\
![](images/Stooge_sort.png)

1. Special Divisors: [task](http://www.rosettacode.org/wiki/Special_Divisors) and [solution](source/Special_Divisors.act)\
![](images/Special_Divisors.png)

1. Special neighbor primes: [task](http://www.rosettacode.org/wiki/Special_neighbor_primes) and [solution](source/Special_neighbor_primes.act)\
![](images/Special_neighbor_primes.png)

1. Spinning rod animation/Text: [task](http://www.rosettacode.org/wiki/Spinning_rod_animation/Text) and [solution](source/Spinning_rod_animation_text.act)\
![](images/Spinning_rod_animation_text.png)

1. Spiral matrix: [task](http://www.rosettacode.org/wiki/Spiral_matrix) and [solution](source/Spiral_matrix.act)\
![](images/Spiral_matrix.png)

1. Split a character string based on change of character: [task](http://www.rosettacode.org/wiki/Split_a_character_string_based_on_change_of_character) and [solution](source/Split_a_character_string_based_on_change_of_character.act)\
![](images/Split_a_character_string_based_on_change_of_character.png)

1. Square but not cube: [task](http://www.rosettacode.org/wiki/Square_but_not_cube) and [solution](source/Square_but_not_cube.act)\
![](images/Square_but_not_cube.png)

1. Stack: [task](http://www.rosettacode.org/wiki/Stack) and [solution1](source/Stack_array.act) [solution2](source/Stack_dynamic.act)\
![](images/Stack_array.png)

1. Statistics/Basic: [task](http://www.rosettacode.org/wiki/Statistics/Basic) and [solution](source/Statistics_basic.act)\
![](images/Statistics_basic.png)

1. Stem-and-leaf plot: [task](http://www.rosettacode.org/wiki/Stem-and-leaf_plot) and [solution](source/Stem-and-leaf_plot.act)\
![](images/Stem-and-leaf_plot.png)

1. Stern-Brocot sequence: [task](http://www.rosettacode.org/wiki/Stern-Brocot_sequence) and [solution](source/Stern-Brocot_sequence.act)\
![](images/Stern-Brocot_sequence.png)

1. Strange numbers: [task](http://www.rosettacode.org/wiki/Strange_numbers) and [solution](source/Strange_numbers.act)\
![](images/Strange_numbers.png)

1. Strange plus numbers: [task](http://www.rosettacode.org/wiki/Strange_plus_numbers) and [solution](source/Strange_plus_numbers.act)\
![](images/Strange_plus_numbers.png)

1. Strange unique prime triplets: [task](http://www.rosettacode.org/wiki/Strange_unique_prime_triplets) and [solution](source/Strange_unique_prime_triplets.act)\
![](images/Strange_unique_prime_triplets.png)

1. String append: [task](http://www.rosettacode.org/wiki/String_append) and [solution](source/String_append.act)\
![](images/String_append.png)

1. String case: [task](http://www.rosettacode.org/wiki/String_case) and [solution](source/String_case.act)\
![](images/String_case.png)

1. String comparison: [task](http://www.rosettacode.org/wiki/String_comparison) and [solution](source/String_comparison.act)\
![](images/String_comparison.png)

1. String concatenation: [task](http://www.rosettacode.org/wiki/String_concatenation) and [solution](source/String_concatenation.act)\
![](images/String_concatenation.png)

1. String interpolation (included): [task](http://www.rosettacode.org/wiki/String_interpolation_(included)) and [solution](source/String_interpolation_(included).act)\
![](images/String_interpolation_(included).png)

1. String length: [task](http://www.rosettacode.org/wiki/String_length) and [solution](source/String_length.act)\
![](images/String_length.png)

1. String matching: [task](http://www.rosettacode.org/wiki/String_matching) and [solution](source/String_matching.act)\
![](images/String_matching.png)

1. String prepend: [task](http://www.rosettacode.org/wiki/String_prepend) and [solution](source/String_prepend.act)\
![](images/String_prepend.png)

1. Strip a set of characters from a string: [task](http://www.rosettacode.org/wiki/Strip_a_set_of_characters_from_a_string) and [solution](source/Strip_a_set_of_characters_from_a_string.act)\
![](images/Strip_a_set_of_characters_from_a_string.png)

1. Strip comments from a string: [task](http://www.rosettacode.org/wiki/Strip_comments_from_a_string) and [solution](source/Strip_comments_from_a_string.act)\
![](images/Strip_comments_from_a_string.png)

1. Strip control codes and extended characters from a string: [task](http://www.rosettacode.org/wiki/Strip_control_codes_and_extended_characters_from_a_string) and [solution](source/Strip_control_codes_and_extended_characters_from_a_string.act)\
![](images/Strip_control_codes_and_extended_characters_from_a_string.png)

1. Strip whitespace from a string/Top and tail: [task](http://www.rosettacode.org/wiki/Strip_whitespace_from_a_string/Top_and_tail) and [solution](source/Strip_whitespace_from_a_string_top_and_tail.act)\
![](images/Strip_whitespace_from_a_string_top_and_tail.png)

1. Subset sum problem: [task](http://www.rosettacode.org/wiki/Subset_sum_problem) and [solution](source/Subset_sum_problem.act)\
![](images/Subset_sum_problem.png)

1. Substring: [task](http://www.rosettacode.org/wiki/Substring) and [solution](source/Substring.act)\
![](images/Substring.png)

1. Substring primes: [task](http://www.rosettacode.org/wiki/Substring_primes) and [solution](source/Substring_primes.act)\
![](images/Substring_primes.png)

1. Substring/Top and tail: [task](http://www.rosettacode.org/wiki/Substring/Top_and_tail) and [solution](source/Top_and_tail.act)\
![](images/Top_and_tail.png)

1. Sum and product of an array: [task](http://www.rosettacode.org/wiki/Sum_and_product_of_an_array) and [solution](source/Sum_and_product_of_an_array.act)\
![](images/Sum_and_product_of_an_array.png)

1. Sum digits of an integer: [task](http://www.rosettacode.org/wiki/Sum_digits_of_an_integer) and [solution](source/Sum_digits_of_an_integer.act)\
![](images/Sum_digits_of_an_integer.png)

1. Sum multiples of 3 and 5: [task](http://www.rosettacode.org/wiki/Sum_multiples_of_3_and_5) and [solution](source/Sum_multiples_of_3_and_5.act)\
![](images/Sum_multiples_of_3_and_5.png)

1. Sum of elements below main diagonal of matrix: [task](http://www.rosettacode.org/wiki/Sum_of_elements_below_main_diagonal_of_matrix) and [solution](source/Sum_of_elements_below_main_diagonal_of_matrix.act)\
![](images/Sum_of_elements_below_main_diagonal_of_matrix.png)

1. Sum of a series: [task](http://www.rosettacode.org/wiki/Sum_of_a_series) and [solution](source/Sum_of_a_series.act)\
![](images/Sum_of_a_series.png)

1. Sum of divisors: [task](http://www.rosettacode.org/wiki/Sum_of_divisors) and [solution](source/Sum_of_divisors.act)\
![](images/Sum_of_divisors.png)

1. Sum of first n cubes: [task](http://www.rosettacode.org/wiki/Sum_of_first_n_cubes) and [solution](source/Sum_of_first_n_cubes.act)\
![](images/Sum_of_first_n_cubes.png)

1. Sum of primes in odd positions is prime: [task](http://www.rosettacode.org/wiki/Sum_of_primes_in_odd_positions_is_prime) and [solution](source/Sum_of_primes_in_odd_positions_is_prime.act)\
![](images/Sum_of_primes_in_odd_positions_is_prime.png)

1. Sum of the digits of n is substring of n: [task](http://www.rosettacode.org/wiki/Sum_of_the_digits_of_n_is_substring_of_n) and [solution](source/Sum_of_the_digits_of_n_is_substring_of_n.act)\
![](images/Sum_of_the_digits_of_n_is_substring_of_n.png)

1. Sum of squares: [task](http://www.rosettacode.org/wiki/Sum_of_squares) and [solution](source/Sum_of_squares.act)\
![](images/Sum_of_squares.png)

1. Sunflower fractal: [task](http://www.rosettacode.org/wiki/Sunflower_fractal) and [solution](source/Sunflower_fractal.act)\
![](images/Sunflower_fractal.png)

1. Superellipse: [task](http://www.rosettacode.org/wiki/Superellipse) and [solution](source/Superellipse.act)\
![](images/Superellipse.png)

1. Symmetric difference: [task](http://www.rosettacode.org/wiki/Symmetric_difference) and [solution](source/Symmetric_difference.act)\
![](images/Symmetric_difference.png)

1. Tau function: [task](http://www.rosettacode.org/wiki/Tau_function) and [solution](source/Tau_function.act)\
![](images/Tau_function.png)

1. Tau number: [task](http://www.rosettacode.org/wiki/Tau_number) and [solution](source/Tau_number.act)\
![](images/Tau_number.png)

1. Temperature conversion: [task](http://www.rosettacode.org/wiki/Temperature_conversion) and [solution](source/Temperature_conversion.act)\
![](images/Temperature_conversion.png)

1. Terminal control/Clear the screen: [task](http://www.rosettacode.org/wiki/Terminal_control/Clear_the_screen) and [solution](source/Clear_the_screen.act)\
![](images/Clear_the_screen.png)

1. Terminal control/Cursor movement: [task](http://www.rosettacode.org/wiki/Terminal_control/Cursor_movement) and [solution](source/Cursor_movement.act)\
![](images/Cursor_movement.png)

1. Terminal control/Cursor positioning: [task](http://www.rosettacode.org/wiki/Terminal_control/Cursor_positioning) and [solution](source/Cursor_positioning.act)\
![](images/Cursor_positioning.png)

1. Terminal control/Dimensions: [task](http://www.rosettacode.org/wiki/Terminal_control/Dimensions) and [solution](source/Terminal_control_dimensions.act)\
![](images/Terminal_control_dimensions.png)

1. Terminal control/Display an extended character: [task](http://www.rosettacode.org/wiki/Terminal_control/Display_an_extended_character) and [solution](source/Display_an_extended_character.act)\
![](images/Display_an_extended_character.png)

1. Terminal control/Hiding the cursor: [task](http://www.rosettacode.org/wiki/Terminal_control/Hiding_the_cursor) and [solution](source/Hiding_the_cursor.act)\
![](images/Hiding_the_cursor.png)

1. Terminal control/Inverse video: [task](http://www.rosettacode.org/wiki/Terminal_control/Inverse_video) and [solution](source/Inverse_video.act)\
![](images/Inverse_video.png)

1. Terminal control/Positional read: [task](http://www.rosettacode.org/wiki/Terminal_control/Positional_read) and [solution](source/Positional_read.act)\
![](images/Positional_read.png)

1. Terminal control/Preserve screen: [task](http://www.rosettacode.org/wiki/Terminal_control/Preserve_screen) and [solution](source/Preserve_screen.act)\
![](images/Preserve_screen.png)

1. Terminal control/Ringing the terminal bell: [task](http://www.rosettacode.org/wiki/Terminal_control/Ringing_the_terminal_bell) and [solution](source/Ringing_the_terminal_bell.act)\
![](images/Ringing_the_terminal_bell.png)

1. Ternary logic: [task](http://www.rosettacode.org/wiki/Ternary_logic) and [solution](source/Ternary_logic.act)\
![](images/Ternary_logic.png)

1. Text between: [task](http://www.rosettacode.org/wiki/Text_between) and [solution](source/Text_between.act)\
![](images/Text_between.png)

1. The Twelve Days of Christmas: [task](http://www.rosettacode.org/wiki/The_Twelve_Days_of_Christmas) and [solution](source/The_Twelve_Days_of_Christmas.act)\
![](images/The_Twelve_Days_of_Christmas.png)

1. Thue-Morse: [task](http://www.rosettacode.org/wiki/Thue-Morse) and [solution](source/Thue-Morse.act)\
![](images/Thue-Morse.png)

1. Time a function: [task](http://www.rosettacode.org/wiki/Time_a_function) and [solution](source/Time_a_function.act)\
![](images/Time_a_function.png)

1. Tokenize a string: [task](http://www.rosettacode.org/wiki/Tokenize_a_string) and [solution](source/Tokenize_a_string.act)\
![](images/Tokenize_a_string.png)

1. Tokenize a string with escaping: [task](http://www.rosettacode.org/wiki/Tokenize_a_string_with_escaping) and [solution](source/Tokenize_a_string_with_escaping.act)\
![](images/Tokenize_a_string_with_escaping.png)

1. Top rank per group: [task](http://www.rosettacode.org/wiki/Top_rank_per_group) and [solution](source/Top_rank_per_group.act)\
![](images/Top_rank_per_group.png)

1. Tree traversal: [task](http://www.rosettacode.org/wiki/Tree_traversal) and [solution](source/Tree_traversal.act)\
![](images/Tree_traversal.png)

1. Triplet of three numbers: [task](http://www.rosettacode.org/wiki/Triplet_of_three_numbers) and [solution](source/Triplet_of_three_numbers.act)\
![](images/Triplet_of_three_numbers.png)

1. Truncate a file: [task](http://www.rosettacode.org/wiki/Truncate_a_file) and [solution](source/Truncate_a_file.act)\
![](images/Truncate_a_file.png)

1. Two identical strings: [task](http://www.rosettacode.org/wiki/Two_identical_strings) and [solution](source/Two_identical_strings.act)\
![](images/Two_identical_strings.png)

1. Two sum: [task](http://www.rosettacode.org/wiki/Two_sum) and [solution](source/Two_sum.act)\
![](images/Two_sum.png)

1. Ulam numbers: [task](http://www.rosettacode.org/wiki/Ulam_numbers) and [solution](source/Ulam_numbers.act)\
![](images/Ulam_numbers.png)

1. Unique characters: [task](http://www.rosettacode.org/wiki/Unique_characters) and [solution](source/Unique_characters.act)\
![](images/Unique_characters.png)

1. Unique characters in each string: [task](http://www.rosettacode.org/wiki/Unique_characters_in_each_string) and [solution](source/Unique_characters_in_each_string.act)\
![](images/Unique_characters_in_each_string.png)

1. UPC: [task](http://www.rosettacode.org/wiki/UPC) and [solution](source/UPC.act)\
![](images/UPC.png)

1. URL decoding: [task](http://www.rosettacode.org/wiki/URL_decoding) and [solution](source/URL_decoding.act)\
![](images/URL_decoding.png)

1. URL encoding: [task](http://www.rosettacode.org/wiki/URL_encoding) and [solution](source/URL_encoding.act)\
![](images/URL_encoding.png)

1. User input/Text: [task](http://www.rosettacode.org/wiki/User_input/Text) and [solution](source/User_input_Text.act)\
![](images/User_input_Text.png)

1. UTF-8 encode and decode: [task](http://www.rosettacode.org/wiki/UTF-8_encode_and_decode) and [solution](source/UTF-8_encode_and_decode.act)\
![](images/UTF-8_encode_and_decode.png)

1. Van der Corput sequence: [task](http://www.rosettacode.org/wiki/Van_der_Corput_sequence) and [solution](source/Van_der_Corput_sequence.act)\
![](images/Van_der_Corput_sequence.png)

1. Van Eck sequence: [task](http://www.rosettacode.org/wiki/Van_Eck_sequence) and [solution](source/Van_Eck_sequence.act)\
![](images/Van_Eck_sequence.png)

1. Vector: [task](http://www.rosettacode.org/wiki/Vector) and [solution](source/Vector.act)\
![](images/Vector.png)

1. Vector products: [task](http://www.rosettacode.org/wiki/Vector_products) and [solution](source/Vector_products.act)\
![](images/Vector_products.png)

1. Vibrating rectangles: [task](http://www.rosettacode.org/wiki/Vibrating_rectangles) and [solution](source/Vibrating_rectangles.act)\
![](images/Vibrating_rectangles.png)

1. Video display modes: [task](http://www.rosettacode.org/wiki/Video_display_modes) and [solution](source/Video_display_modes.act)\
![](images/Video_display_modes.png)

1. Vigenère cipher: [task](http://www.rosettacode.org/wiki/Vigen%C3%A8re_cipher) and [solution](source/Vigenère_cipher.act)\
![](images/Vigenère_cipher.png)

1. Walk a directory/Non-recursively: [task](http://www.rosettacode.org/wiki/Walk_a_directory/Non-recursively) and [solution](source/Walk_a_directory_non-recursively.act)\
![](images/Walk_a_directory_non-recursively.png)

1. Water collected between towers: [task](http://www.rosettacode.org/wiki/Water_collected_between_towers) and [solution](source/Water_collected_between_towers.act)\
![](images/Water_collected_between_towers.png)

1. Word wrap: [task](http://www.rosettacode.org/wiki/Word_wrap) and [solution](source/Word_wrap.act)\
![](images/Word_wrap.png)

1. Words containing "the" substring: [task](http://www.rosettacode.org/wiki/Words_containing_%22the%22_substring) and [solution](source/Words_containing_the_substring.act)\
![](images/Words_containing_the_substring.png)

1. Write entire file: [task](http://www.rosettacode.org/wiki/Write_entire_file) and [solution](source/Write_entire_file.act)\
![](images/Write_entire_file.png)

1. Write language name in 3D ASCII: [task](http://www.rosettacode.org/wiki/Write_language_name_in_3D_ASCII) and [solution](source/Write_language_name_in_3D_ASCII.act)\
![](images/Write_language_name_in_3D_ASCII.png)

1. Xiaolin Wu's line algorithm: [task](http://www.rosettacode.org/wiki/Xiaolin_Wu%27s_line_algorithm) and [solution](source/Xiaolin_Wu's_line_algorithm.act)\
![](images/Xiaolin_Wu's_line_algorithm.png)

1. XML/Output: [task](http://www.rosettacode.org/wiki/XML/Output) and [solution](source/XML_Output.act)\
![](images/XML_Output.png)

1. Yellowstone sequence: [task](http://www.rosettacode.org/wiki/Yellowstone_sequence) and [solution](source/Yellowstone_sequence.act)\
![](images/Yellowstone_sequence.png)

1. Yin and yang: [task](http://www.rosettacode.org/wiki/Yin_and_yang) and [solution](source/Yin_and_yang.act)\
![](images/Yin_and_yang.png)

1. Zeckendorf number representation: [task](http://www.rosettacode.org/wiki/Zeckendorf_number_representation) and [solution](source/Zeckendorf_number_representation.act)\
![](images/Zeckendorf_number_representation.png)

1. Zero to the zero power: [task](http://www.rosettacode.org/wiki/Zero_to_the_zero_power) and [solution](source/Zero_to_the_zero_power.act)\
![](images/Zero_to_the_zero_power.png)

1. Zhang-Suen thinning algorithm: [task](http://www.rosettacode.org/wiki/Zhang-Suen_thinning_algorithm) and [solution](source/Zhang-Suen_thinning_algorithm.act)\
![](images/Zhang-Suen_thinning_algorithm.png)

1. Zig-zag matrix: [task](http://www.rosettacode.org/wiki/Zig-zag_matrix) and [solution](source/Zig-zag_matrix.act)\
![](images/Zig-zag_matrix.png)